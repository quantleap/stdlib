package princeton.stdlib.visual;

public class VisualStats {

  /**
   * Plot points (i, a[i]) to standard draw.
   */
  public static void plotPoints(double[] a) {
      int N = a.length;
      StdDraw.setXscale(0, N-1);
      StdDraw.setPenRadius(1.0 / (3.0 * N));
      for (int i = 0; i < N; i++) {
          StdDraw.point(i, a[i]);
      }
  }

 /**
   * Plot line segments connecting points (i, a[i]) to standard draw.
   */
  public static void plotLines(double[] a) {
      int N = a.length;
      StdDraw.setXscale(0, N-1);
      StdDraw.setPenRadius();
      for (int i = 1; i < N; i++) {
          StdDraw.line(i-1, a[i-1], i, a[i]);
      }
  }

 /**
   * Plot bars from (0, a[i]) to (i, a[i]) to standard draw.
   */
  public static void plotBars(double[] a) {
      int N = a.length;
      StdDraw.setXscale(0, N-1);
      for (int i = 0; i < N; i++) {
          StdDraw.filledRectangle(i, a[i]/2, .25, a[i]/2);
      }
  }

}
